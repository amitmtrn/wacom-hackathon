import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UpCommandManager {

	public static void main(String[] args) {

    UpCommandManager obj = new UpCommandManager();

    final String faceKey = "1";
    final String lightKey = "2";
    final String[] faceCommand = new String[]{"cmd", "/c","start chrome facebook.com"};

       if (args[0] != null && !args[0].equals("")){
         switch(args[0]){
           case faceKey:
             obj.executeCommand(faceCommand);
             break;
         }
       }
  }
  private void executeCommand(String[] command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader =
                            new BufferedReader(new InputStreamReader(p.getInputStream()));

                        String line = "";
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
